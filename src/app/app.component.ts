
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AlfrescoApi, AlfrescoApiConfig, NodesApi, NodePaging } from '@alfresco/js-api';


declare const Viewer: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  products!: any;
  
  
  alfrescoApi: any;
  folders: any;
  documents: any;

  @ViewChild('viewerContainer') viewerContainer!: ElementRef;

  constructor() {
    let alfrescoApiConfig = new AlfrescoApiConfig({
      hostEcm: 'http://192.168.15.202:8080',
      authType: 'BASIC',
      oauth2: false,
      contextRoot: ''
    });

    this.alfrescoApi = new AlfrescoApi(alfrescoApiConfig);

    let username = 'user';
    let password = 'user';
    let folderId = '341d199f-c9a1-4c67-8376-5b9b6df09d91'; //-root- o ID de la carpeta raíz, puedes cambiarlo según tu estructura de carpetas

    this.alfrescoApi.login(username, password).then((data: any) => {
      console.log('Autenticación exitosa ', data);
      // Aquí puedes realizar acciones después de la autenticación exitosa
      this.listFolderChildren(folderId);

    }).catch((error: any) => {
      console.error('Error de autenticación', error);
      // Manejo de errores de autenticación
    });




  }
  ngAfterViewInit(): void {
    
    /////usar viewer
    if (this.viewerContainer) {
      let viewer = new Viewer(this.viewerContainer.nativeElement);
      // Configuraciones adicionales de Viewer.js
      viewer.open('http://192.168.15.202:8080/#/preview/s/isRBZ_wZRIS8tFM4QfQqhw');

    }
  }
  ngOnInit(): void {
    this.products = [
      {
          id: '1000',
          code: 'f230fh0g3',
          name: 'Bamboo Watch',
          description: 'Product Description',
          image: 'bamboo-watch.jpg',
          price: 65,
          category: 'Accessories',
          quantity: 24,
          inventoryStatus: 'INSTOCK',
          rating: 5
      },
      {
          id: '1001',
          code: 'nvklal433',
          name: 'Black Watch',
          description: 'Product Description',
          image: 'black-watch.jpg',
          price: 72,
          category: 'Accessories',
          quantity: 61,
          inventoryStatus: 'OUTOFSTOCK',
          rating: 4
      }]
  }



  listFolderChildren(folderId: string) {
    let nodesApi = new NodesApi(this.alfrescoApi);

    nodesApi.listNodeChildren(folderId).then((data: NodePaging) => {
      this.folders = data.list.entries.filter(entry => entry.entry.isFolder).map(entry => entry.entry);
      console.log('Carpetas:', this.folders);
    }).catch((error) => {
      console.error('Error al listar carpetas', error);
      // Manejo de errores al listar carpetas
    });
  }


  /* listar(folderId:any){
    
     this.alfrescoService.getDocumentsInFolder(folderId).then((documents) => {
       this.documents = documents;
       console.log("documents",documents);
       
     });
   }*/

  getNodesInFolder(folderId: string) {
    let nodesApi = new NodesApi(this.alfrescoApi);

    nodesApi.listNodeChildren(folderId).then((documents) => {
      this.documents = documents.list.entries;
      console.log(documents);
    }).catch((error) => {
      console.error(error);
    });
  }

  mostrarPDF() {

  }

}
